# Atom User Settings

## Packages

### atom-ide-ui

Configure **git-bash** on Windows.

`$HOME/.nuclide-terminal.json`:
```json
{
  "command": [
    "bash.exe",
    "--login"
  ]
}
```

### vim-mode-plus
